package cen4072;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import cen4072.models.SideNavigationBar;
import cen4072.models.SignInPage;
import cen4072.models.TradePage;

@RunWith(JUnit4.class)
public class TradeTests {
  private static WebDriver driver;

  @BeforeClass
  public static void onetimeSetup() {
    // Create a new instance of the Firefox driver
    // Notice that the remainder of the code relies on the interface,
    // not the implementation.
    driver = new FirefoxDriver();
  }

  @Before
  public void setup() {
    new SignInPage(driver)
        .open()
        .signinDefaultCredentials();
  }

  @After
  public void teardown() {
    new SideNavigationBar(driver)
        .logout();
  }

  @AfterClass
  public static void onetimeTeardown() {
    driver.quit();
  }

  @Test
  public void tradePage_buyWithInvalidSymbol_ExpectErrorMessage() {
    new SideNavigationBar(driver)
        .clickTradeLink();

    String errorMsg = new TradePage(driver)
        .enterSymbol("INVALID")
        .enterQuantity("1")
        .clickPlaceTradeButton()
        .getErrorMessage();

    Assert.assertEquals("The ticker symbol you entered is not valid. Please try again.", errorMsg);
  }
}
