package cen4072.models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SideNavigationBar {
  private WebDriver driver;

  public SideNavigationBar(WebDriver driver) {
    this.driver = driver;
  }

  public void clickTradeLink() {
    WebElement tradeLink = driver.findElement(By.name("trade"));
    tradeLink.click();
  }

  public void logout() {
    WebElement logoutLink = driver.findElement(By.name("logout"));
    logoutLink.click();
  }
}
