package cen4072.models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TradePage {
  private WebDriver driver;

  public TradePage(WebDriver driver) {
    this.driver = driver;
  }

  public TradePage enterSymbol(String symbol) {
    WebElement symbolTextField = driver.findElement(By.name("symbol"));
    symbolTextField.clear();
    symbolTextField.sendKeys(symbol);

    return this;
  }

  public TradePage enterQuantity(String quantity) {
    WebElement quantityTextField = driver.findElement(By.name("quantity"));
    quantityTextField.clear();
    quantityTextField.sendKeys(quantity);

    return this;
  }

  public TradePage clickPlaceTradeButton() {
    WebElement placeTradeButton = driver.findElement(By.cssSelector("input[type=\"image\"]"));
    placeTradeButton.click();

    return this;
  }

  public String getErrorMessage() {
    WebElement errorMessage = driver.findElement(By.xpath("//form/table/tbody/tr[2]/td"));
    return errorMessage.getText();
  }
}
